package com.bbva.uuaa.batch;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.bbva.uuaa.dto.banco.CuentaDTO;
import com.bbva.uuaa.lib.rj01.UUAARJ01;

public class ReaderBDD  implements ItemReader<CuentaDTO>{

	private UUAARJ01 uuaaRJ01;
	Iterator <CuentaDTO> cuentaDTOIterator;
	
	private static final Logger LOGGER=LoggerFactory.getLogger(ReaderBDD.class);
	


	public void setUuaaRJ01(UUAARJ01 uuaaRJ01) {
		this.uuaaRJ01 = uuaaRJ01;
	}

	public void before() {
		LOGGER.info("pasamos por el before por la consulta a la base");
		List<CuentaDTO>listCuentas=uuaaRJ01.excecuteGetAccount();
		cuentaDTOIterator=listCuentas.iterator();
	}
	@Override
	
	public CuentaDTO read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException{
	
		LOGGER.info("pasamos por el reader");
		
		if(cuentaDTOIterator !=null && cuentaDTOIterator.hasNext()) {
			return cuentaDTOIterator.next();
		}
		return null;
	}
}
